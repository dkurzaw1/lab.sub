---
layout: post
current: post
cover: assets/images/dariah-status-graph.png
navigation: True
title: Static Infrastructure Status with Jekyll and GitHub Pages
date: 2018-02-13 12:53:00
tags: prototype tools dariah jekyll
class: post-template
subclass: 'post tag-tools'
author: cthiel
---

## Technology fails – at least sometimes.

This is particularly true for a modern distributed research infrastructure, such as [DARIAH-DE](https://de.dariah.eu/).

For the operation of this infrastructure, we have implemented a monitoring solution with [Icinga](https://www.icinga.com/).
This enables us to be informed about problems and react quickly.
In an ideal state, our world looks like this:
![monitoring](assets/images/dariah-status-monitoring.jpg)

As well as this works, the problems we face are manifold:
1. Users don't know if someone is already working on a red icon here.
2. This map shows only the current state, but sometimes maintenances must be announced in advance.
3. The monitoring itself is not entirely independent from the infrastructure.

Usually, our engineers are quick in identifying problems and fixing them.
But if users start querying them about the status of the issue they are working on, it won't get fixed quicker.

So we decided to build an independent and manually curated status page.

## GitHub status page

The first approach, was to have a static HTML file on GitHub pages with [current information](https://dariah-de.github.io/status/).
The advantages of this approach are:
* The status page is entirely independent from our infrastructure and reasonably reliant.
* Security-wise, static pages are probably the best you can do.
* Authentication and authorisation are handled entirely outside our infrastructure.
But since it's GitHub, we feel reasonably safe to assume that everyone will have their credentials at hand even in a stressful event such as a datacenter outage.

This page was well-received by users and is now checked regularly by our power users in case of problems, instead of picking up the phone immediately.

![status](assets/images/dariah-status-OK.png)

However, as the infrastructure grows, the number of dependencies increases and sometimes it is hard to list all services affected by a single outage.

## Now powered by Jekyll

We decided to include the service dependencies directly into the GitHub page by utilising [Jekyll's Collections](https://jekyllrb.com/docs/collections/).

The services, middlewares and infrastructure components are now modelled in `yaml` datasets.

```yaml
title: 'Example Service'
dependencies:
  - '/infrastructure/storagesystem'
  - '/servers/mymachine'
```

So in case of emergencies, we can add the actually broken parts to a Jekyll data file.

```yaml
- title: 'The storage is full!'
  description: "Don't worry, we ordered a few new hard drives."
  date_start: '2018-02-12 11:01'
  affected:
    - '/infrastructure/storagesystem'
```

Then, using recursive liquid templates, we get a list of all of the affected services.
In the above example, it would list at least *Example Service* but any other services that (indirectly) depends on *storagesystem*.

![status](assets/images/dariah-status-exampleerror.png)

Of course, the same approach is possible for announcements of upcoming maintenances.
For more details on the implementation, see the [official documentation](https://dariah-de.github.io/status/documentation.html)
or the [source code](https://github.com/DARIAH-DE/status).

## Status quo

The implementation is relatively new and not yet complete as of writing this article.
![status](assets/images/dariah-status-graph.png)
The graph shows the currently known dependencies.
Following a `git push`, GitHub will generate the new page, as long as all dependencies can be resolved.
In parallel, [Travis-CI](https://travis-ci.org/) will rebuild the dependency graph and compare it to the committed version using [NetworkX](https://networkx.github.io/).

Rebuilding the graph and populating the [history](https://dariah-de.github.io/status/history.html) is still manual,
as this reduces complexity and future-proofs the tool for changes in the infrastructure that would break dependency resolution for historical data.

This solution was also the topic of a [FOSDEM'18 lightning talk](https://fosdem.org/2018/schedule/event/static_infrastructure_status/) by the author:

<video preload="none" controls="controls">
  <source src="https://video.fosdem.org/2018/H.2215/static_infrastructure_status.webm" type='video/webm; codecs="vp8, vorbis"' />
  Your browser seems not to support video embedding!
</video>





