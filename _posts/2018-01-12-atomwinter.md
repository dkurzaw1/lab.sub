---
layout: post
current: post
cover: assets/images/atom-testgrid.png
cover-credits: CC BY 4.0 Mathias
navigation: True
title: Hacking the hackable editor
date: 2018-01-17 00:00:01
tags: prototype tools atom textgrid
class: post-template
subclass: 'post tag-tools'
author: mathias
---
Looking for platforms to start prototyping the Atom text editor or its superior platform Electron passed our way at the Research and Development Department recently. For feasibility studies we sometimes test new frameworks and when we can use web technologies to start coding, there is quite a good chance for a seamless connection between data structures, algorithms and the front end.

# Atom
In 2014 GitHub released *Atom* ([v0.0.1](https://github.com/atom/atom/tags?after=v0.4.0)) including a marketing offensive with a corporate design, idiom wording (e.g. the manual is called 'Flight Manual') and followed by a [video at release of version 1.0](https://www.youtube.com/watch?v=Y7aEiVwBAdk).
They subtitled the editor as »a hackable text editor« and this is where we start
today, when Atom is likely one of the TOP 5 editors in an overall ranking based
on Wikipedia's [Comparison of text editors](https://en.wikipedia.org/wiki/Comparison_of_text_editors).
On a single day my colleagues and me were looking at this tool. Some of us
already use the editor day by day, others had no experience so we took a short
tour through the UI and looked behind the curtain after.

## GitHub
Needless to say that Atom comes with an outstanding integration for git and GitHub.
Within the UI staged and unstaged changes are just one click away and in a view
together with an additional form field for a commit message. The status bar
provides buttons to pull/push and
some operations on branches. Only for heavier tasks (for example when using *gitflow*)
one has to return to the command line. When using one of the add-on packages – which
everyone can publish at the major repository – what is thrown as a fatal error is
immediately compared to what may be found on the corresponding issue tracker at
GitHub. This helps to collect errors from the users, but also gives a feedback
when an issue is already tracked. :punch:

## …a browser! It's a BROWSER!
In fact Atom is a browser. A Chromium with the V8 JavaScript engine. And the kickstarter
package is just about presenting a modal with a somewhat strange message.
A package allows you to customize many parts of Atom like the menu bar, the
context menu you get on right click. This is where it far exceeds the browser.
A browser does not have access to the filesystem on behalf of the user – and we
all know why.
In addition with Atom it is also possible to listen to a global
`window.onBeforeUnload` event, so you can do stuff before Atom shuts down.

## configuration
Sometimes very good things are pretty simple. In our daily business we always try
to reduce or automatize common tasks.

### key shortcuts
Working with a basic text editor keyboard
shortcuts can increase our productivity. The key-bindings are very easy to
configure. There is a UI and for the pleasure of assembler-programmer
there is also a [json](https://www.json.org/) or
[cson](https://github.com/bevry/cson) file available.

### snippets
Same thing here. Setup your own snippets via menu `Edit` »» `Snippets…`.
Here is an example for a complete TEI document, i just have to enter `doc-tei`.
![snippets](assets/images/atom-snippet.png)

## init script
There is also the chance to add some instructions to the startup routine: the
`init.cs` (or a `init.js` replacement).
We can teach Atom some \[e.ti.kɛt\]:
```javascript
alert("Hello friend");
```
But the whole Atom-API is exposed to this, so instead of an alert that needs a
user interaction we can use an even more friendly notification:
```javascript
atom.notifications.addInfo("Hi there");
```
We can imagine coding many things in here, like presenting the latest *XKCD* (or
*Dilbert* if you are older then 40 years). But when it comes to more complicated
tasks we should think of getting out of the init script and prepare a complete
plug-in: A package.

## Packages
There are some tutorials out there, for example [here](http://flight-manual.atom.io/hacking-atom/sections/package-word-count/) and [here](https://github.com/blog/2231-building-your-first-atom-plugin).
But there is also a build in *Package Generator* to get you kickstarted. A
package may contains predefined keybindings and snippets.

### menu
You can easily create new menu items, not only at the `Packages` menu but also on
the top level.

### UI elements
Atom provides some parts of the user interface. Modals, alerts and notifications are mentioned here before. The main views are called panes and docks (like the status bar) can be placed on the border of the window.

## lets get our hands dirty
We collected some ideas of things we like to do with Atom.

### IIIF in Atom
In summer we presented our IIIF implementation at our [digitalization center](https://gdz.sub.uni-goettingen.de/).
May a IIIF validation engine with the ability to preview the images described by
a manifest with our image viewer [TIFY](https://github.com/subugoe/tify) within
Atom?

### GND search
Our what about supporting metadata encoding with the [GND](http://www.dnb.de/DE/Standardisierung/GND/gnd_node.html), the major
authority file in Germany that provides RDF structured data. We can imagine a
nice little implementation for querying this resource and providing results with
some templates.

### TextGrid
Since some of the participants are familiar to the [TextGrid](https://textgrid.de)-API we started to
code some parts of what we have available in the RCP called *TextGrid Laboratory*.
We need to integrate the Authentification and we can do some basic operations
like read and write or lock an object. The powerful editor combined with a
reliable storage => sounds good.

#### Navigator
Within the [Eclipse](https://www.eclipse.org/) based *TextGrid Laboratory* the
Navigator is a kind of a file browser that presents a file/folder like structure
of objects. For a basic implementation without being able to use other components
we have to provide two informations via the settings of our package:
1. the session id (in the absence of shibboleth) and
2. an entry point to start browsing the resources (in absence of tg-auth that provides a list of projects per user).

#### settings
With Atom it is derisory compared to other frameworks to achieve something like this.
Atoms built in package manager is able to manage all this. All you need is an object placed at the `package.json` - the main package config file.
```json
"configSchema": {
    "sessionId": {
      "type": "string",
      "default": "",
      "title": "sessionId",
      "description": "To obtain access to non-public resources, a valid SessionId is needed."
    },
    "aggregation": {
      "type": "string",
      "default": "",
      "title": "URI (Aggregation)",
      "description": "FOR DEV ONLY: provide an entry aggregation."
    }
}
```
![package settings view](assets/images/atom-packageSettings.png)

#### navigator view
All we have to do is getting the needed informations from our REST-API and we
have to display the result somewhere.
```javascript
navigator() {
  let el = document.createElement("div");
  el.setAttribute("class", "navigator");
  const navigator = {
    element: el,
    getTitle: () => 'TextGrid Navigator',
    getURI: () => 'atom://textgrid/navigator',
    getDefaultLocation: () => 'left',
    onDidOpen: this.navigatorPrepare()
  };
  atom.workspace.open(navigator, {split:"down"});
}
```

*Somewhere* is `atom.workspace.open()`. When the view is available we query for
items and list them. Everything else is a matter of event listeners and
recursion. What we get is the ability to browse through collections stored in
the non-public area of the TextGrid Repository.
![TestGrid Navigator](assets/images/atom-testgrid-navigator.png)

We are still working to support the authentification component as well to open
the gate to more services and we will provide the package at least in a public
git repo.
