---
layout: post
current: post
cover: assets/images/twitter.jpg
cover-credits: By Mark Kent - originally posted to Flickr as Secratary Bird - Strutting Stuff, CC BY-SA 2.0, https://commons.wikimedia.org/w/index.php?curid=9690412
navigation: True
title: Twitter for Goettingen Digitisation Centre
date: 2018-01-18 00:00:01
tags: gdz symfony
class: post-template
subclass: 'post tag-gdz-twiter'
author: ipf
---

## Twitter for Goettingen Digitisation Centre / Göttinger Digitalisierungszentrum

Did you know, that with the current relaunch of the [GDZ](https://gdz.sub.uni-goettingen.de) (Göttinger Digitalisierungszentrum), we implemented a small bridge to Twitter that
tweets out the newly scanned works?

This feature was integrated in our Symfony App using the simple [endroid/twitter](https://github.com/endroid/twitter) library.

Feel free to follow this unofficial account at [@just_scanned](https://twitter.com/just_scanned).