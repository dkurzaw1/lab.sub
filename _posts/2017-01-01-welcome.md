---
layout: post
current: post
cover: assets/images/Bibliothek_der_UNI_Göttingen-wikiCommons-Suhaknoke.jpg
cover-credits: CC BY-SA 4.0 WikiCommons:Suhaknoke
navigation: True
title: Welcome to the Lab
date: 2017-01-01 00:00:01
tags:
class: post-template
subclass: 'post tag-getting-started'
author: Team
---

Welcome everyone!

This is the place to read about projects, resources and all the things we deal
with at our work on digital collections, databases, APIs, tools and esoteric
programming languages we prefer to write in. It is *not* meant for a single
subject, but covers all topics we 💖. So i hope you
don't mind if the next post is about one of my adorable cute rabbits.
